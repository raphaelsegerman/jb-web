<?php
/**
 * jb-web functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package jb-web
 */

if ( ! function_exists( 'jb_web_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function jb_web_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on jb-web, use a find and replace
		 * to change 'jb-web' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'jb-web', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'jb-web' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'jb_web_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'jb_web_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jb_web_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'jb_web_content_width', 640 );
}
add_action( 'after_setup_theme', 'jb_web_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jb_web_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jb-web' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'jb-web' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

    register_sidebar( array(
        'name' => 'Footer Widget 1',
        'id' => 'footer-widget-1',
        'description' => 'Appears in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget footer-widget">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => 'Footer Widget 2',
        'id' => 'footer-widget-2',
        'description' => 'Appears in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget footer-widget">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => 'Footer Widget 3',
        'id' => 'footer-widget-3',
        'description' => 'Appears in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget footer-widget">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}
add_action( 'widgets_init', 'jb_web_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function jb_web_scripts() {
    wp_enqueue_style( 'fonts', get_template_directory_uri() . '/assets/css/fonts.css' );

    wp_enqueue_style( 'grid', get_template_directory_uri() . '/assets/css/bootstrap-grid.css' );

    wp_enqueue_style( 'swiper-style', get_template_directory_uri() . '/assets/css/swiper.min.css' );

	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );

    wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/assets/js/vendor/swiper.min.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), '20151215', true );
}
add_action( 'wp_enqueue_scripts', 'jb_web_scripts' );

add_image_size( 'journal-featured-thumb', 1120, 600, true );
add_image_size( 'journal-thumb', 400, 360, true );

add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;

    ob_start();

    ?>
    <a class="cart-link" href="<?php echo home_url(); ?>/cart">
        <svg width="30px" height="22px" viewBox="0 0 30 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="Artboard-8" transform="translate(-1522.000000, -52.000000)" fill="#404041">
                    <g id="Group-1236" transform="translate(49.000000, 38.830000)">
                        <path d="M1497.2877,13.6069 L1493.2257,27.8119 L1478.3127,27.8119 L1479.3067,30.1799 L1494.9777,30.1799 L1499.0907,15.9739 L1501.3737,15.9739 L1502.2537,13.6069 L1497.2877,13.6069 Z M1491.4047,25.4449 L1493.7457,17.1579 L1473.8427,17.1579 L1477.3207,25.4449 L1491.4047,25.4449 Z M1489.8237,31.3639 C1488.8437,31.3639 1488.0487,32.1579 1488.0487,33.1389 C1488.0487,34.1209 1488.8437,34.9149 1489.8237,34.9149 C1490.8047,34.9149 1491.5997,34.1209 1491.5997,33.1389 C1491.5997,32.1589 1490.8047,31.3639 1489.8237,31.3639 L1489.8237,31.3639 Z M1485.6807,33.1389 C1485.6807,34.1209 1484.8857,34.9149 1483.9047,34.9149 C1482.9247,34.9149 1482.1297,34.1209 1482.1297,33.1389 C1482.1297,32.1589 1482.9247,31.3639 1483.9047,31.3639 C1484.8857,31.3639 1485.6807,32.1589 1485.6807,33.1389 L1485.6807,33.1389 Z" id="Fill-1234"></path>
                    </g>
                </g>
            </g>
        </svg>

        <?php if (WC()->cart->get_cart_contents_count() > 0) { ?>
            <div class="cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></div>
        <?php } ?>
    </a>

    <?php

    $fragments['a.cart-link'] = ob_get_clean();

    return $fragments;

}