jQuery(function($) {

    // Mobile Navigation

    let toggle = $('.menu-toggle');

    toggle.on('click', function () {
        $('html').addClass('mobile-menu-active');
    });

    $('.menu-close').on('click', function () {
        $('html').removeClass('mobile-menu-active');
    });

    $('.main-navigation a').on('click', function () {
        $('html').removeClass('mobile-menu-active');
    });

    $(window).on( 'resize', function() {
       if ($(this).width() > 992) {
           $('html').removeClass('mobile-menu-active');
       }
    });


    // Hide Header on Scroll Down but Show when Scroll Up

    if($(window).scrollTop() > 0) {
        $('body').addClass('scrolled');
    } else {
        $('body').removeClass('scrolled');
    }

    $(window).on( 'scroll', function() {
        if($(window).scrollTop() > 0) {
            $('body').addClass('scrolled');
        } else {
            $('body').removeClass('scrolled');
        }
    });

    let didScroll;
    let lastScrollTop = 0;
    let delta = 1;
    let navbarHeight = '';

    $(window).load( function() {
        navbarHeight = $('.site-header').outerHeight();
    });

    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        let st = $(this).scrollTop();

        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        if (st > lastScrollTop && st > navbarHeight){
            $('.site-header').addClass('nav-up');
        } else {
            if(st + $(window).height() < $(document).height()) {
                $('.site-header').removeClass('nav-up');
            }
        }

        if (st < 300){
            $('.site-header').removeClass('nav-up');
        }

        lastScrollTop = st;
    }


    // Menu contact link smooth scrolling

    $(document).on('click', '.menu-contact-us a', function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 1500);
    });


    // Beer links smooth scrolling

    $(document).on('click', '.our-beers-beer-link', function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 1500);
    });


    // Back to top smooth scrolling

    $(document).on('click', '.back-to-top', function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $('body').offset().top
        }, 1500);
    });


    // Brewery smooth scrolling

    $(document).on('click', '.menu-brewery .sub-menu a', function (e) {
        e.preventDefault();

        let section = $.attr(this, 'href');

        if ($('body').hasClass('page-template-template-brewery')) {
            $('html, body').animate({
                scrollTop: $(section).offset().top
            }, 1500);
        } else {
            location.href = '/brewery' + section;
        }
    });

    $(document).on('click', '.menu-story .sub-menu a', function (e) {
        e.preventDefault();

        let section = $.attr(this, 'href');

        if ($('body').hasClass('page-template-template-story')) {
            $('html, body').animate({
                scrollTop: $(section).offset().top
            }, 1500);
        } else {
            location.href = '/story' + section;
        }
    });


    // Mobile Navigation

    let searchToggle = $('.search-link');

    searchToggle.on('click', function (e) {
        e.preventDefault();
        $('html').addClass('search-active');
    });

    $('.search-close').on('click', function () {
        $('html').removeClass('search-active');
    });

    $(window).on( 'resize', function() {
        if ($(this).width() > 992) {
            $('html').removeClass('search-active');
        }
    });


    // Brewery Slider

    var mySwiper = new Swiper ('.swiper-container', {
        loop: true,
        speed: 800,
        autoplay: {
            delay: 5000,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
    });


    let beerDrag = function () {
        if ($(window).width() > 992 && $('body').hasClass('page-template-template-beer')) {
            const slider = document.querySelector('.our-beers-inner');
            let isDown = false;
            let startX;
            let scrollLeft;

            slider.addEventListener('mousedown', (e) => {
                isDown = true;
                slider.classList.add('active');
                startX = e.pageX - slider.offsetLeft;
                scrollLeft = slider.scrollLeft;
            });
            slider.addEventListener('mouseleave', () => {
                isDown = false;
                slider.classList.remove('active');
            });
            slider.addEventListener('mouseup', () => {
                isDown = false;
                slider.classList.remove('active');
            });
            slider.addEventListener('mousemove', (e) => {
                if(!isDown) return;
                e.preventDefault();
                const x = e.pageX - slider.offsetLeft;
                const walk = (x - startX) * 1; //scroll-fast
                slider.scrollLeft = scrollLeft - walk;
            });
        }
    };

    beerDrag();

    $(window).on( 'resize', function() {
        beerDrag();
    });
});

if (window.location.hash) {
    let hash = window.location.hash;

    jQuery("html, body").animate(
        {
            scrollTop: jQuery(hash).offset().top
        },
        2000
    );
}