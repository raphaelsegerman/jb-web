<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package jb-web
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

            <section class="banner-section">
                <div class="banner-section-background" style="background-image: url('<?php echo get_template_directory_uri(). '/assets/img/journal/banner.jpg' ?>');"></div>

                <div class="banner-section-content-wrap">
                    <h1 class="banner-section-title">Our Journal</h1>

                    <p class="banner-section-content">A collection of stories, interviews and announcements honoring the people and places that inspire us.</p>

                    <a href="#" class="btn">EXPLORE POSTS</a>
                </div>
            </section>

            <div class="journal-section">
                <div class="container">
                    <div class="sidebar-wrapper">
                        <div class="sidebar-left posts">
                            <?php
                            if ( have_posts() ) :
                                $count = 0;
                                while ( have_posts() ) : the_post();

                                    $excerpt = get_the_excerpt();

                                    $excerpt = substr($excerpt, 0, 200);
                                    $result = substr($excerpt, 0, strrpos($excerpt, ' '));


                                    $count++;

                                    if ($count == 1) { ?>

                                        <div class="post post-featured">
                                            <div class="post-image">
                                                <img src="<?php echo get_the_post_thumbnail_url('', 'journal-featured-thumb'); ?>">
                                            </div>

                                            <div class="post-inner">
                                                <div class="post-left">
                                                    <h3 class="post-title">
                                                        <a class="btn" href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                                    </h3>

                                                    <span class="post-date">
                                                    <?php echo get_the_date( 'F j' ); ?>
                                                </span>
                                                </div>

                                                <div class="post-right">
                                                    <p class="post-excerpt"><?php echo $result . '...'; ?></p>

                                                    <a href="<?php echo get_the_permalink(); ?>" class="btn btn-red">Read Full Article</a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } else { ?>

                                        <div class="post">
                                            <div class="post-inner">
                                                <div class="post-left">
                                                    <div class="post-image">
                                                        <img src="<?php echo get_the_post_thumbnail_url('', 'journal-thumb'); ?>">
                                                    </div>
                                                </div>

                                                <div class="post-right">
                                                    <h3 class="post-title">
                                                        <a class="btn" href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                                    </h3>

                                                    <span class="post-date">
                                                        <?php echo get_the_date( 'F j' ); ?>
                                                    </span>

                                                    <p class="post-excerpt"><?php echo $result . '...'; ?></p>

                                                    <a href="<?php echo get_the_permalink(); ?>" class="btn btn-red">Read Full Article</a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php }
                                endwhile;

                                the_posts_pagination();

                            endif;
                            ?>

                            <?php wp_reset_query(); ?>
                        </div>

                        <div class="sidebar-right">
                            <svg class="sidebar-pattern" width="1041px" height="44px" viewBox="0 0 1041 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Group-6" fill="#E6E7E8">
                                        <path d="M617.255,8.62 L617.255,1.713 L615.208,-2.27373675e-13 L606.953,-2.27373675e-13 L617.255,8.62 Z M617.255,24.778 L617.255,17.871 L595.898,-2.27373675e-13 L587.644,-2.27373675e-13 L617.255,24.778 Z M617.255,40.936 L617.255,34.029 L576.588,-2.27373675e-13 L568.333,-2.27373675e-13 L617.255,40.936 Z M600.602,43.158 L608.856,43.158 L557.278,-2.27373675e-13 L549.024,-2.27373675e-13 L600.602,43.158 Z M581.291,43.158 L589.546,43.158 L537.968,-2.27373675e-13 L529.714,-2.27373675e-13 L581.291,43.158 Z M561.982,43.158 L570.236,43.158 L518.658,-2.27373675e-13 L510.403,-2.27373675e-13 L561.982,43.158 Z M542.672,43.158 L550.926,43.158 L499.349,-2.27373675e-13 L491.094,-2.27373675e-13 L542.672,43.158 Z M523.361,43.158 L531.616,43.158 L480.038,-2.27373675e-13 L471.784,-2.27373675e-13 L523.361,43.158 Z M504.052,43.158 L512.307,43.158 L460.729,-2.27373675e-13 L452.474,-2.27373675e-13 L504.052,43.158 Z M484.741,43.158 L492.996,43.158 L441.418,-2.27373675e-13 L433.164,-2.27373675e-13 L484.741,43.158 Z M465.432,43.158 L473.687,43.158 L422.109,-2.27373675e-13 L413.854,-2.27373675e-13 L465.432,43.158 Z M446.122,43.158 L454.377,43.158 L402.799,-2.27373675e-13 L394.544,-2.27373675e-13 L446.122,43.158 Z M426.812,43.158 L435.067,43.158 L383.488,-2.27373675e-13 L375.234,-2.27373675e-13 L426.812,43.158 Z M407.502,43.158 L415.757,43.158 L364.179,-2.27373675e-13 L355.924,-2.27373675e-13 L407.502,43.158 Z M388.193,43.158 L396.446,43.158 L344.869,-2.27373675e-13 L336.614,-2.27373675e-13 L388.193,43.158 Z M368.882,43.158 L377.137,43.158 L325.559,-2.27373675e-13 L317.305,-2.27373675e-13 L368.882,43.158 Z M349.572,43.158 L357.827,43.158 L306.249,-2.27373675e-13 L297.994,-2.27373675e-13 L349.572,43.158 Z M330.262,43.158 L338.517,43.158 L286.94,-2.27373675e-13 L278.685,-2.27373675e-13 L330.262,43.158 Z M310.952,43.158 L319.207,43.158 L267.629,-2.27373675e-13 L259.375,-2.27373675e-13 L310.952,43.158 Z M291.643,43.158 L299.897,43.158 L248.319,-2.27373675e-13 L240.065,-2.27373675e-13 L291.643,43.158 Z M272.332,43.158 L280.587,43.158 L229.01,-2.27373675e-13 L220.755,-2.27373675e-13 L272.332,43.158 Z M253.023,43.158 L261.277,43.158 L209.699,-2.27373675e-13 L201.445,-2.27373675e-13 L253.023,43.158 Z M233.713,43.158 L241.967,43.158 L190.39,-2.27373675e-13 L182.135,-2.27373675e-13 L233.713,43.158 Z M214.402,43.158 L222.657,43.158 L171.079,-2.27373675e-13 L162.825,-2.27373675e-13 L214.402,43.158 Z M195.093,43.158 L203.348,43.158 L151.77,-2.27373675e-13 L143.515,-2.27373675e-13 L195.093,43.158 Z M175.783,43.158 L184.037,43.158 L132.46,-2.27373675e-13 L124.205,-2.27373675e-13 L175.783,43.158 Z M156.473,43.158 L164.728,43.158 L113.15,-2.27373675e-13 L104.896,-2.27373675e-13 L156.473,43.158 Z M137.163,43.158 L145.418,43.158 L93.84,-2.27373675e-13 L85.585,-2.27373675e-13 L137.163,43.158 Z M117.854,43.158 L126.108,43.158 L74.53,-2.27373675e-13 L66.276,-2.27373675e-13 L117.854,43.158 Z M98.543,43.158 L106.798,43.158 L55.22,-2.27373675e-13 L46.966,-2.27373675e-13 L98.543,43.158 Z M79.234,43.158 L87.488,43.158 L35.91,-2.27373675e-13 L27.655,-2.27373675e-13 L79.234,43.158 Z M59.924,43.158 L68.178,43.158 L16.601,-2.27373675e-13 L8.346,-2.27373675e-13 L59.924,43.158 Z M40.613,43.158 L48.868,43.158 L4.54747351e-13,2.267 L4.54747351e-13,9.174 L40.613,43.158 Z M21.304,43.158 L29.559,43.158 L4.54747351e-13,18.425 L4.54747351e-13,25.332 L21.304,43.158 Z M4.54747351e-13,41.49 L4.54747351e-13,34.583 L10.248,43.158 L1.993,43.158 L4.54747351e-13,41.49 Z" id="Fill-1213-Copy"></path>
                                        <path d="M1040.711,8.62 L1040.711,1.713 L1038.664,-3.55271368e-15 L1030.409,-3.55271368e-15 L1040.711,8.62 Z M1040.711,24.778 L1040.711,17.871 L1019.354,6.80344669e-13 L1011.1,6.80344669e-13 L1040.711,24.778 Z M1040.711,40.936 L1040.711,34.029 L1000.044,4.54747351e-13 L991.789,4.54747351e-13 L1040.711,40.936 Z M1024.058,43.158 L1032.312,43.158 L980.734,2.23820962e-13 L972.48,2.23820962e-13 L1024.058,43.158 Z M1004.747,43.158 L1013.002,43.158 L961.424,2.23820962e-13 L953.17,2.23820962e-13 L1004.747,43.158 Z M985.438,43.158 L993.692,43.158 L942.114,2.23820962e-13 L933.859,2.23820962e-13 L985.438,43.158 Z M966.128,43.158 L974.382,43.158 L922.805,2.23820962e-13 L914.55,2.23820962e-13 L966.128,43.158 Z M946.817,43.158 L955.072,43.158 L903.494,2.23820962e-13 L895.24,2.23820962e-13 L946.817,43.158 Z M927.508,43.158 L935.763,43.158 L884.185,2.23820962e-13 L875.93,2.23820962e-13 L927.508,43.158 Z M908.197,43.158 L916.452,43.158 L864.874,2.23820962e-13 L856.62,2.23820962e-13 L908.197,43.158 Z M888.888,43.158 L897.143,43.158 L845.565,2.23820962e-13 L837.31,2.23820962e-13 L888.888,43.158 Z M869.578,43.158 L877.833,43.158 L826.255,2.23820962e-13 L818,2.23820962e-13 L869.578,43.158 Z M850.713,43.158 L858.967,43.158 L807.39,4.51194637e-13 L799.135,4.51194637e-13 L850.713,43.158 Z M831.402,43.158 L839.657,43.158 L788.079,4.51194637e-13 L779.825,4.51194637e-13 L831.402,43.158 Z M812.093,43.158 L820.348,43.158 L768.77,4.51194637e-13 L760.515,4.51194637e-13 L812.093,43.158 Z M792.783,43.158 L801.037,43.158 L749.46,4.51194637e-13 L741.205,4.51194637e-13 L792.783,43.158 Z M773.473,43.158 L781.728,43.158 L730.15,4.51194637e-13 L721.896,4.51194637e-13 L773.473,43.158 Z M754.163,43.158 L762.418,43.158 L710.84,4.51194637e-13 L702.585,4.51194637e-13 L754.163,43.158 Z M734.854,43.158 L743.108,43.158 L691.53,4.51194637e-13 L683.276,4.51194637e-13 L734.854,43.158 Z M715.543,43.158 L723.798,43.158 L672.22,4.51194637e-13 L663.966,4.51194637e-13 L715.543,43.158 Z M696.234,43.158 L704.488,43.158 L652.91,4.51194637e-13 L644.655,4.51194637e-13 L696.234,43.158 Z M676.924,43.158 L685.178,43.158 L633.601,4.51194637e-13 L625.346,4.51194637e-13 L676.924,43.158 Z M657.613,43.158 L665.868,43.158 L617,2.267 L617,9.174 L657.613,43.158 Z M638.304,43.158 L646.559,43.158 L617,18.425 L617,25.332 L638.304,43.158 Z M617,41.49 L617,34.583 L627.248,43.158 L618.993,43.158 L617,41.49 Z" id="Fill-1213-Copy-2"></path>
                                    </g>
                                </g>
                            </svg>

                            <div class="sidebar">
                                <div class="sidebar-widget">
                                    <h3 class="sidebar-widget-title">Categories</h3>

                                    <div class="sidebar-widget-content">
                                        <?php

                                        $categories = get_categories( array(
                                            'orderby' => 'name',
                                        ) );

                                        foreach ( $categories as $category ) { ?>
                                            <a href="<?php echo home_url() . '/category/' . $category->slug; ?>" class="sidebar-link"><?php echo $category->name ?></a>
                                            <?php
                                        } ?>
                                    </div>
                                </div>

                                <div class="sidebar-widget">
                                    <h3 class="sidebar-widget-title">Recent Stories</h3>

                                    <div class="sidebar-widget-content">
                                        <?php

                                        $args = array(
                                            'post_type' => 'post',
                                            'posts_per_page' => 3,
                                        );

                                        $query = new WP_Query( $args );

                                        if ($query->have_posts()) :
                                            $count = 0;
                                            while ($query->have_posts()) : $query->the_post(); ?>

                                                <a href="<?php echo get_the_permalink(); ?>" class="sidebar-link"><?php echo get_the_title(); ?></a>

                                            <?php endwhile;
                                        endif;

                                        wp_reset_query();
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
