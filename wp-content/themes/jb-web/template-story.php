<?php
/**
 * Template Name: Story
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

            <section class="banner-section">
                <div class="banner-section-background" style="background-image: url('<?php echo get_template_directory_uri(). '/assets/img/story/ross.jpg' ?>');"></div>

                <div class="banner-section-content-wrap">
                    <h1 class="banner-section-title">A STORY ABOUT BEER.</h1>

                    <p class="banner-section-content">It all started with a crazy dream to bring South Africans the kind of beer they deserved. The kind of beer that was made slow, made right. The kind of beer you’d wanna tell you friends about, because it was that good.</p>

                    <a href="<?php echo home_url(); ?>/beer" class="btn">OUR BEERS</a>
                </div>
            </section>

            <section class="landscape standard-content-section">
                <div class="container">
                    <div class="section-header section-header-fancy">
                        <h2 class="section-title">Changing the craft beer</h2>

                        <div class="section-header-inner">
                            <svg class="section-header-pattern" width="245px" height="44px" viewBox="0 0 245 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <defs>
                                    <polygon id="path-1" points="0 0.5603 244.684 0.5603 244.684 43.7193 0 43.7193"></polygon>
                                </defs>
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Artboard-2" transform="translate(0.000000, -2775.000000)">
                                        <g id="Group-318" transform="translate(0.000000, 2774.700000)">
                                            <mask id="mask-2" fill="white">
                                                <use xlink:href="#path-1"></use>
                                            </mask>
                                            <g id="Clip-317"></g>
                                            <path d="M308.6277,9.1803 L308.6277,2.2733 L306.5807,0.5603 L298.3257,0.5603 L308.6277,9.1803 Z M308.6277,25.3383 L308.6277,18.4313 L287.2707,0.5603 L279.0157,0.5603 L308.6277,25.3383 Z M308.6277,41.4963 L308.6277,34.5893 L267.9607,0.5603 L259.7057,0.5603 L308.6277,41.4963 Z M291.9737,43.7193 L300.2287,43.7193 L248.6507,0.5603 L240.3957,0.5603 L291.9737,43.7193 Z M272.6637,43.7193 L280.9187,43.7193 L229.3407,0.5603 L221.0857,0.5603 L272.6637,43.7193 Z M253.3537,43.7193 L261.6087,43.7193 L210.0307,0.5603 L201.7767,0.5603 L253.3537,43.7193 Z M234.0447,43.7193 L242.2987,43.7193 L190.7207,0.5603 L182.4667,0.5603 L234.0447,43.7193 Z M214.7337,43.7193 L222.9887,43.7193 L171.4107,0.5603 L163.1567,0.5603 L214.7337,43.7193 Z M195.4247,43.7193 L203.6787,43.7193 L152.1007,0.5603 L143.8467,0.5603 L195.4247,43.7193 Z M176.1147,43.7193 L184.3687,43.7193 L132.7907,0.5603 L124.5367,0.5603 L176.1147,43.7193 Z M156.8047,43.7193 L165.0587,43.7193 L113.4807,0.5603 L105.2267,0.5603 L156.8047,43.7193 Z M137.4947,43.7193 L145.7487,43.7193 L94.1717,0.5603 L85.9167,0.5603 L137.4947,43.7193 Z M118.1847,43.7193 L126.4397,43.7193 L74.8617,0.5603 L66.6067,0.5603 L118.1847,43.7193 Z M98.8747,43.7193 L107.1287,43.7193 L55.5517,0.5603 L47.2967,0.5603 L98.8747,43.7193 Z M79.5647,43.7193 L87.8187,43.7193 L36.2417,0.5603 L27.9867,0.5603 L79.5647,43.7193 Z M60.2547,43.7193 L68.5097,43.7193 L16.9317,0.5603 L8.6767,0.5603 L60.2547,43.7193 Z M40.9447,43.7193 L49.1997,43.7193 L-2.3783,0.5603 L-10.6333,0.5603 L40.9447,43.7193 Z M21.6347,43.7193 L29.8897,43.7193 L-21.6883,0.5603 L-29.9433,0.5603 L21.6347,43.7193 Z M2.3247,43.7193 L10.5797,43.7193 L-40.9983,0.5603 L-49.2523,0.5603 L2.3247,43.7193 Z M-16.9853,43.7193 L-8.7303,43.7193 L-60.3083,0.5603 L-68.5623,0.5603 L-16.9853,43.7193 Z M-36.2953,43.7193 L-28.0403,43.7193 L-79.6183,0.5603 L-87.8723,0.5603 L-36.2953,43.7193 Z M-55.6043,43.7193 L-47.3503,43.7193 L-98.9283,0.5603 L-107.1823,0.5603 L-55.6043,43.7193 Z M-74.9143,43.7193 L-66.6603,43.7193 L-118.2383,0.5603 L-126.4923,0.5603 L-74.9143,43.7193 Z M-94.2243,43.7193 L-85.9703,43.7193 L-137.5483,0.5603 L-145.8023,0.5603 L-94.2243,43.7193 Z M-113.5343,43.7193 L-105.2803,43.7193 L-156.8573,0.5603 L-165.1123,0.5603 L-113.5343,43.7193 Z M-132.8443,43.7193 L-124.5903,43.7193 L-176.1673,0.5603 L-184.4223,0.5603 L-132.8443,43.7193 Z M-152.1543,43.7193 L-143.9003,43.7193 L-195.4773,0.5603 L-203.7323,0.5603 L-152.1543,43.7193 Z M-171.4643,43.7193 L-163.2093,43.7193 L-214.7873,0.5603 L-223.0423,0.5603 L-171.4643,43.7193 Z M-190.7743,43.7193 L-182.5193,43.7193 L-234.0973,0.5603 L-242.3523,0.5603 L-190.7743,43.7193 Z M-210.0843,43.7193 L-201.8293,43.7193 L-253.4073,0.5603 L-261.6623,0.5603 L-210.0843,43.7193 Z M-229.3943,43.7193 L-221.1393,43.7193 L-272.7173,0.5603 L-280.9713,0.5603 L-229.3943,43.7193 Z M-248.7043,43.7193 L-240.4493,43.7193 L-292.0273,0.5603 L-300.2813,0.5603 L-248.7043,43.7193 Z M-268.0143,43.7193 L-259.7593,43.7193 L-308.6273,2.8273 L-308.6273,9.7343 L-268.0143,43.7193 Z M-287.3233,43.7193 L-279.0693,43.7193 L-308.6273,18.9853 L-308.6273,25.8923 L-287.3233,43.7193 Z M-308.6273,42.0503 L-308.6273,35.1433 L-298.3793,43.7193 L-306.6333,43.7193 L-308.6273,42.0503 Z" id="Fill-316" fill="#E6E7E8" mask="url(#mask-2)"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <h3 class="section-subtitle">Landscape</h3>
                        </div>
                    </div>

                    <div class="standard-content-section-inner">
                        <p class="content-large">In 2007, fuelled with passion, perseverance, and little else – at a time when the words “craft beer” were foreign to most – founders Ross and Meg McCulloch set out to change the beer landscape in South Africa.</p>

                        <p class="content-small">Jack Black’s has since grown to become one of Africa’s most sought after and well-loved craft beers. Today, we’re as passionate about quality as we are our people. We raise a glass to our communities near and far, to those suiting up, gearing up, slowing down and falling forward – we got your back. Because hey, who wouldn’t do it, if not for the sake of a good beer. Cheers!</p>
                    </div>
                </div>
            </section>

            <section class="image-slider-section image-slider-section-alt swiper-container">
                <div class="swiper-wrapper">
                    <div class="image-slider-item swiper-slide" style="background-image: url('<?php echo get_field('story_slider_image_1'); ?>');"></div>
                    <div class="image-slider-item swiper-slide" style="background-image: url('<?php echo get_field('story_slider_image_2'); ?>');"></div>
                    <div class="image-slider-item swiper-slide" style="background-image: url('<?php echo get_field('story_slider_image_3'); ?>');"></div>
                    <div class="image-slider-item swiper-slide" style="background-image: url('<?php echo get_field('story_slider_image_4'); ?>');"></div>
                </div>

                <div class="swiper-button-prev">
                    <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="iconmonstr-arrow-27" transform="translate(16.000000, 16.000000) rotate(-180.000000) translate(-16.000000, -16.000000) " fill="#000000" fill-rule="nonzero">
                                <path d="M16,0 C7.164,0 0,7.164 0,16 C0,24.836 7.164,32 16,32 C24.836,32 32,24.836 32,16 C32,7.164 24.836,0 16,0 Z M14.376,25.3333333 L12,23 L19,16 L12,9 L14.376,6.66666667 L23.6666667,16 L14.376,25.3333333 Z" id="Shape"></path>
                            </g>
                        </g>
                    </svg>
                </div>

                <div class="swiper-button-next">
                    <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="iconmonstr-arrow-27" transform="translate(16.000000, 16.000000) scale(-1, 1) rotate(-180.000000) translate(-16.000000, -16.000000) " fill="#000000" fill-rule="nonzero">
                                <path d="M16,0 C7.164,0 0,7.164 0,16 C0,24.836 7.164,32 16,32 C24.836,32 32,24.836 32,16 C32,7.164 24.836,0 16,0 Z M14.376,25.3333333 L12,23 L19,16 L12,9 L14.376,6.66666667 L23.6666667,16 L14.376,25.3333333 Z" id="Shape"></path>
                            </g>
                        </g>
                    </svg>
                </div>

                <div class="swiper-pagination"></div>
            </section>

            <section id="ambassadors-section" class="ambassadors standard-content-section section-dark">
                <div class="container">
                    <div class="section-header section-header-fancy">
                        <h2 class="section-title">Introducing</h2>

                        <div class="section-header-inner">
                            <svg class="section-header-pattern" width="245px" height="44px" viewBox="0 0 245 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <defs>
                                    <polygon id="path-1" points="0 0.5603 244.684 0.5603 244.684 43.7193 0 43.7193"></polygon>
                                </defs>
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Artboard-2" transform="translate(0.000000, -2775.000000)">
                                        <g id="Group-318" transform="translate(0.000000, 2774.700000)">
                                            <mask id="mask-2" fill="white">
                                                <use xlink:href="#path-1"></use>
                                            </mask>
                                            <g id="Clip-317"></g>
                                            <path d="M308.6277,9.1803 L308.6277,2.2733 L306.5807,0.5603 L298.3257,0.5603 L308.6277,9.1803 Z M308.6277,25.3383 L308.6277,18.4313 L287.2707,0.5603 L279.0157,0.5603 L308.6277,25.3383 Z M308.6277,41.4963 L308.6277,34.5893 L267.9607,0.5603 L259.7057,0.5603 L308.6277,41.4963 Z M291.9737,43.7193 L300.2287,43.7193 L248.6507,0.5603 L240.3957,0.5603 L291.9737,43.7193 Z M272.6637,43.7193 L280.9187,43.7193 L229.3407,0.5603 L221.0857,0.5603 L272.6637,43.7193 Z M253.3537,43.7193 L261.6087,43.7193 L210.0307,0.5603 L201.7767,0.5603 L253.3537,43.7193 Z M234.0447,43.7193 L242.2987,43.7193 L190.7207,0.5603 L182.4667,0.5603 L234.0447,43.7193 Z M214.7337,43.7193 L222.9887,43.7193 L171.4107,0.5603 L163.1567,0.5603 L214.7337,43.7193 Z M195.4247,43.7193 L203.6787,43.7193 L152.1007,0.5603 L143.8467,0.5603 L195.4247,43.7193 Z M176.1147,43.7193 L184.3687,43.7193 L132.7907,0.5603 L124.5367,0.5603 L176.1147,43.7193 Z M156.8047,43.7193 L165.0587,43.7193 L113.4807,0.5603 L105.2267,0.5603 L156.8047,43.7193 Z M137.4947,43.7193 L145.7487,43.7193 L94.1717,0.5603 L85.9167,0.5603 L137.4947,43.7193 Z M118.1847,43.7193 L126.4397,43.7193 L74.8617,0.5603 L66.6067,0.5603 L118.1847,43.7193 Z M98.8747,43.7193 L107.1287,43.7193 L55.5517,0.5603 L47.2967,0.5603 L98.8747,43.7193 Z M79.5647,43.7193 L87.8187,43.7193 L36.2417,0.5603 L27.9867,0.5603 L79.5647,43.7193 Z M60.2547,43.7193 L68.5097,43.7193 L16.9317,0.5603 L8.6767,0.5603 L60.2547,43.7193 Z M40.9447,43.7193 L49.1997,43.7193 L-2.3783,0.5603 L-10.6333,0.5603 L40.9447,43.7193 Z M21.6347,43.7193 L29.8897,43.7193 L-21.6883,0.5603 L-29.9433,0.5603 L21.6347,43.7193 Z M2.3247,43.7193 L10.5797,43.7193 L-40.9983,0.5603 L-49.2523,0.5603 L2.3247,43.7193 Z M-16.9853,43.7193 L-8.7303,43.7193 L-60.3083,0.5603 L-68.5623,0.5603 L-16.9853,43.7193 Z M-36.2953,43.7193 L-28.0403,43.7193 L-79.6183,0.5603 L-87.8723,0.5603 L-36.2953,43.7193 Z M-55.6043,43.7193 L-47.3503,43.7193 L-98.9283,0.5603 L-107.1823,0.5603 L-55.6043,43.7193 Z M-74.9143,43.7193 L-66.6603,43.7193 L-118.2383,0.5603 L-126.4923,0.5603 L-74.9143,43.7193 Z M-94.2243,43.7193 L-85.9703,43.7193 L-137.5483,0.5603 L-145.8023,0.5603 L-94.2243,43.7193 Z M-113.5343,43.7193 L-105.2803,43.7193 L-156.8573,0.5603 L-165.1123,0.5603 L-113.5343,43.7193 Z M-132.8443,43.7193 L-124.5903,43.7193 L-176.1673,0.5603 L-184.4223,0.5603 L-132.8443,43.7193 Z M-152.1543,43.7193 L-143.9003,43.7193 L-195.4773,0.5603 L-203.7323,0.5603 L-152.1543,43.7193 Z M-171.4643,43.7193 L-163.2093,43.7193 L-214.7873,0.5603 L-223.0423,0.5603 L-171.4643,43.7193 Z M-190.7743,43.7193 L-182.5193,43.7193 L-234.0973,0.5603 L-242.3523,0.5603 L-190.7743,43.7193 Z M-210.0843,43.7193 L-201.8293,43.7193 L-253.4073,0.5603 L-261.6623,0.5603 L-210.0843,43.7193 Z M-229.3943,43.7193 L-221.1393,43.7193 L-272.7173,0.5603 L-280.9713,0.5603 L-229.3943,43.7193 Z M-248.7043,43.7193 L-240.4493,43.7193 L-292.0273,0.5603 L-300.2813,0.5603 L-248.7043,43.7193 Z M-268.0143,43.7193 L-259.7593,43.7193 L-308.6273,2.8273 L-308.6273,9.7343 L-268.0143,43.7193 Z M-287.3233,43.7193 L-279.0693,43.7193 L-308.6273,18.9853 L-308.6273,25.8923 L-287.3233,43.7193 Z M-308.6273,42.0503 L-308.6273,35.1433 L-298.3793,43.7193 L-306.6333,43.7193 L-308.6273,42.0503 Z" id="Fill-316" fill="#E6E7E8" mask="url(#mask-2)"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <h3 class="section-subtitle">Our Ambassadors</h3>
                        </div>
                    </div>

                    <div class="standard-content-section-inner">
                        <p class="content-large">Ordinary people doing extra-ordinary things.</p>

                        <p class="content-small">While their areas of interest are diverse, there are common threads that bind – not only in terms of values, but also how they live their everyday lives. We hope you’ll enjoy watching this space as much as we’ve enjoyed bringing this group together.</p>
                    </div>

                    <div class="ambassadors-grid">
                        <?php
                        $loop = new WP_Query( array(
                                'post_type' => 'ambassador',
                                'posts_per_page' => -1,
                                'orderby' => 'title',
                                'order'   => 'ASC',
                            )
                        );
                        ?>

                        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                            <div class="ambassador-wrap" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
                                <div class="ambassador-inner">
                                    <a href="<?php echo get_the_permalink(); ?>" class="ambassador-title btn btn-white"><?php echo get_the_title(); ?></a>
                                </div>
                            </div>

                        <?php endwhile; wp_reset_query(); ?>
                    </div>

                    <div class="btn-container">
                        <a href="<?php echo home_url(); ?>/journal" class="btn btn-red">Visit Our Journal</a>
                    </div>
                </div>
            </section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

