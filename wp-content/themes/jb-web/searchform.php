<form role="search" method="get" class="search-form" action="<?php echo home_url(); ?>">
    <label>
        <input type="search" class="search-field" placeholder="Search …" value="" name="s">
    </label>
    <input type="submit" class="btn btn-red search-submit" value="Search">
</form>